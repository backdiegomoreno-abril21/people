FROM openjdk:15-jdk-alpine
COPY target/people-0.0.1.jar api.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/api.jar"]