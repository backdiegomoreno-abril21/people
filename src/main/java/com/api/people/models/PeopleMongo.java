package com.api.people.models;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("client")
public class PeopleMongo {

    public String id;
    public String namne;
    public String lastName;
    public double age;
    public String city;

    public PeopleMongo(String namne, String lastName, double age, String city) {
        this.namne = namne;
        this.lastName = lastName;
        this.age = age;
        this.city = city;
    }

    @Override
    public String toString() {
        return "PeopleMongo{" +
                "id='" + id + '\'' +
                ", namne='" + namne + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

}
