package com.api.people;

import com.api.people.models.PeopleMongo;
import com.api.people.models.PeopleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PeopleController {

    @Autowired
    private PeopleRepository repository;

    /* Get lista de personas */
    @GetMapping(value = "/clients", produces = "application/json")
    public ResponseEntity<List<PeopleMongo>> obtenerListado()
    {
        List<PeopleMongo> list = repository.findAll();
        return new ResponseEntity<List<PeopleMongo>>(list, HttpStatus.OK);
    }

    /* Add nueva persona */
    @PostMapping(value = "/clients", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody PeopleMongo personaNueva) {
        System.out.println("Estoy en añadir");
        System.out.println(personaNueva.toString());
        repository.insert(personaNueva);
        return new ResponseEntity<>("Cliente creado correctamente", HttpStatus.CREATED);
    }
}
