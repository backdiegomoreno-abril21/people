# README #

API Clients. Persistencia con MongoDB.

### Autor ###

* Diego Javier Moreno Rusinque
* Version 1.0

### Docker ###

* [Imagen Docker Hub](https://hub.docker.com/repository/docker/diegojavierrusinque/apiclients)